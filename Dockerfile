LABEL MAINTAINER="Hamilton Santos <hamsantosjose@gmail.com>"
LABEL APP_VERSION="1.0.0"

ENV NPM_VERSION=8 ENVIRONTMENT=PROD

FROM ubuntu

RUN apt-get update && apt-get install -y git nano npm

WORKDIR /usr/share/myapp

RUN npm build

COPY ./files/requeriments.txt requeriments.txt

ADD ./files.tar.gz ./   

RUN useradd hamilton

USER hamilton

EXPOSE 8080

VOLUME [ "/data" ]

ENTRYPOINT ["ping"]

CMD ["localhost"]
